
# заготовка для домашней работы
# прочитайте про glob.glob
# https://docs.python.org/3/library/glob.html

#Программа ожидает строку, которую будет искать (input()).
#После того, как строка введена, программа ищет её во всех файлах, выводит имена найденных файлов построчно
# и выводит количество найденных файлов.
#Программа снова ожидает ввод, но теперь поиск происходит только среди отобранных на предыдущем этапе файлов.
#Программа снова ожидает ввод.


import glob
import os.path


def search_in_file(file, search_str):
    with open(file) as f:
        for line in f:
            if search_str in line:
                return True
                
    return False


def iter_files(files, input_str):
    current_files = []
    num_of_files = 0
    for filename in files:
        if search_in_file(filename, input_str):
            num_of_files += 1
            current_files.append(filename)
            print(filename)
    return current_files


def main():
    dir = 'Advanced Migrations'
    files = glob.glob(os.path.join(dir, "*.sql"))
    current_files = files
    while(True):
        input_str = input('Что будем искать?')
        current_files = iter_files(current_files, input_str)
        print('Найдено {} файлов'.format(len(current_files)))

main()
